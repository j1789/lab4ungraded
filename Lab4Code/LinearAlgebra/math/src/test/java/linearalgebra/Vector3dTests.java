package linearalgebra;

import static org.junit.Assert.*;

import org.junit.Test;

public class Vector3dTests{

    @Test
    public void return_correct_result(){
        Vector3d v = new Vector3d(1,1,2);
        assertEquals(1.0, v.getX(), 0.001);
    }

    @Test
    public void return_correct_magnitude(){
        Vector3d v = new Vector3d(2,2,1);
        assertEquals(3.0, v.magnitudeValue(), 0.001);
    }

    @Test
    public void return_correct_dotProduct(){
        Vector3d v1 = new Vector3d(1,1,2);
        Vector3d v2 = new Vector3d(2, 3, 4);
        assertEquals(13.0, v1.dotProduct(v2), 0.001);
    }

    @Test
    public void return_correct_added(){
        Vector3d v1 = new Vector3d(1,1,2);
        Vector3d v2 = new Vector3d(2, 3, 4);
        assertEquals(3, v1.add(v2).getX(), 0.001);
    }


}
