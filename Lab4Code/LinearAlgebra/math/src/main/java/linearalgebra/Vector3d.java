package linearalgebra;

public class Vector3d{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }

    public double magnitudeValue(){
        double magnitude = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
        return magnitude;
    }
    
    public double dotProduct(Vector3d v){
        double dot = (this.x + v.getX())+(this.y+v.getY())+(this.z+v.getZ());
        return dot;
    }

    public Vector3d add(Vector3d v){
        Vector3d addedV = new Vector3d((this.x+v.getX()), (this.y+v.getY()), (this.z)+v.getZ());
        return addedV;
    }
}